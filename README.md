# Description
InstaWordCloud is a command-line Python3 script that was initially made by [realdenis](https://github.com/realdennis/igcloud) but since it was not working anymore due to Instagram API updates, I adapted it to the new API and also added persian language support.

# Installation
Open terminal and enter the following commands:
```
$ git clone https://gitlab.com/fussyv/instawordcloud.git
$ cd ./instawordcloud
$ pip3 install -r requirements.txt
```

# Usage
```
$ python3 igcloud.py [username]
$ python3 igcloud.py thisisbillgates //for example
```
or
`$ ./igcloud.py [username]`

# Example

![](https://gitlab.com/fussyv/instawordcloud/-/raw/master/Examples/1.png)
![](https://gitlab.com/fussyv/instawordcloud/-/raw/master/Examples/2.png)


